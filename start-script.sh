#!/bin/bash

python -V
python -m pip install --upgrade pip
pip -V
python -m pip install virtualenv

python -m venv venv

source venv/Scripts/activate

pip install -r requirements.txt

status=$?
if [ $status -ne 0 ]
    then
        echo "Requirements installation failed!"
        exit $status
fi

echo "Requirements installed successfully!"

echo "Starting Server........................."
uvicorn main:app --reload