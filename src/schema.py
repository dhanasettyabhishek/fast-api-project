from datetime import date
from pydantic import BaseModel


class Movie(BaseModel):
    id = int
    name = str
    descp = str
    type = str
    url = str
    rating = int

    class Config:
        orm_mode = True
